<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ColocationController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('colocation')->paginate(10);
        return view('admin.colocation.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.colocation.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:colocation,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     	 => (string) $request->name,
            'catalog'  	 => $catalog,
            'fees'     	 => (int) $request->fees,
            'periodic' 	 => (int) $request->periodic,
            'display'  	 => (int) $request->display,
            'note'     	 => (string) $request->note,
            'rack'	     => (string) $request->rack,
            'speed_domestic'  	     => (string) $request->speed_domestic,
            'speed_international'    => (string) $request->speed_international,
            'ip'  		 => (int) $request->ip,
            'services'   => (string) $request->services,
            'link'   => (string) $request->link,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('colocation')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới chỗ đặt server thành công!');
            return redirect()->route('admin.colocation.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới chỗ đặt server không thành công!');
            return redirect()->route('admin.colocation.index');
        }
    }

    public function edit($id)
    {
        $colocation = DB::table('colocation')->where('id', $id)->first();
        if (isset($colocation)) {
            $data['data'] = $colocation;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.colocation.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có chỗ đặt server này!');
        return redirect()->route('admin.colocation.index');
    }

    public function update(Request $request, $id)
    {
       $colocation = DB::table('colocation')->where('id', $id)->first();
        if (isset($colocation)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($colocation->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'          => (string) $request->name,
                'catalog'    => $catalog,
                'fees'       => (int) $request->fees,
                'periodic'   => (int) $request->periodic,
                'display'    => (int) $request->display,
                'note'       => (string) $request->note,
                'rack'       => (string) $request->rack,
                'speed_domestic'         => (string) $request->speed_domestic,
                'speed_international'    => (string) $request->speed_international,
                'ip'         => (int) $request->ip,
                'services'   => (string) $request->services,
                'link'   => (string) $request->link,
                'location'   => $location,
                'created_at' => $date_time,
                'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('colocation')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $colocation->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin chỗ đặt server thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin chỗ đặt server không thành công!');
            }
            return redirect()->route('admin.colocation.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có chỗ đặt server này!');
        return redirect()->route('admin.colocation.index');
    }

    public function delete($id)
    {
        $colocation = DB::table('colocation')->where('id', $id)->first();
        if (isset($colocation)) {
            $function = DB::table('colocation')->where('id', $id)->delete();
            if ($function) {
                if ($colocation->catalog != 0) {
                    DB::table('catalog')->where('id', $colocation->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa chỗ đặt server thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa chỗ đặt server không thành công!');
            }
            return redirect()->route('admin.colocation.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có chỗ đặt server này!');
        return redirect()->route('admin.colocation.index');
    }
}
