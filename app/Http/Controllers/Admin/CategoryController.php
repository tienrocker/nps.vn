<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('category')->paginate(10);
        return view('admin.category.index', $data);
    }

    public function create()
    {
    	$data['list'] = DB::table('category')->select('id', 'name', 'parent_id', 'display')->get();
        return view('admin.category.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:200'
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $data = [
            'name'     => $request->name,
            'slug'     => Str::slug($request->name, '-'),
            'parent_id' => $parent_id,
            'location'  => json_encode($request->location),
            'display'     => $request->display,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $check_slug = DB::table('category')->where('slug', $data['slug'])->first();
		if (isset($check_slug)) {
			Session::flash('status', 'danger');
            Session::flash('notify', 'Danh mục này đã tồn tại, vui lòng chọn tên khác!');
            return redirect()->route('admin.category.add');
		}
        $id = DB::table('category')->insertGetId($data);

        if ($id) {
        	if ($parent_id != 0) {
	    		$arr_cate = DB::table('category')->where('id', $parent_id)->value('child');
				$arr_cate = explode(',', $arr_cate);
				if (empty($arr_cate)) {
					$category_child = [$id];
				}else{
					array_push($arr_cate, $id);
					$category_child = $arr_cate;
				}
				$category_child = array_unique($category_child);
				$category_child = implode(',', $category_child);
				DB::table('category')->where('id', $parent_id)->update(['child' => $category_child]);
	    	}
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới danh mục bài viết thành công!');
            return redirect()->route('admin.category.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới danh mục bài viết không thành công!');
            return redirect()->route('admin.category.index');
        }
    }

    public function edit($id)
    {
        $cate = DB::table('category')->where('id', $id)->first();
        if (isset($cate)) {
            $data['data'] = $cate;
            $data['list'] = DB::table('category')->get();
			if ($cate->child) {
				$data['child'] = explode(',', $cate->child);
			}
            return view('admin.category.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.category.index');
    }

    public function update(Request $request, $id)
    {
       $cate = DB::table('category')->where('id', $id)->first();
        if (isset($cate)) {
            $rules = [
                'name'              => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $data = [
	            'name'     	=> $request->name,
	            'slug'     	=> Str::slug($request->name, '-'),
	            'parent_id' => $parent_id,
	            'location'  => json_encode($request->location),
	            'display'   => $request->display,
	            'child'		=> $request->cate_child,
	            'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('category')->where('id', $id)->update($data);
            if ($function) {
            	if ($parent_id != 0) {
		    		$arr_cate = DB::table('category')->where('id', $parent_id)->value('child');
					$arr_cate = explode(',', $arr_cate);
					if (empty($arr_cate)) {
						$category_child = [$id];
					}else{
						array_push($arr_cate, $id);
						$category_child = $arr_cate;
					}
					$category_child = array_unique($category_child);
					$category_child = implode(',', $category_child);
					DB::table('category')->where('id', $parent_id)->update(['child' => $category_child]);
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin danh mục thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin danh mục không thành công!');
            }
            return redirect()->route('admin.category.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.category.index');
    }

    public function delete($id)
    {
        $category = DB::table('category')->where('id', $id)->where('count_post', 0)->first();
        if (isset($category)) {
			if (!empty($category->child)) {
				Session::flash('status', 'success');
                Session::flash('notify', 'Vui lòng di chuyển những danh mục con ra khỏi danh mục này rồi hãy xóa!');
				return redirect()->route('admin.category.index');
			}
            $function = DB::table('category')->where('id', $id)->delete();
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa danh mục bài viết thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa danh mục bài viết không thành công!');
            }
            return redirect()->route('admin.category.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.category.index');
    }
}
