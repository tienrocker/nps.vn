<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class RackController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('rack')->paginate(10);
        return view('admin.rack.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.rack.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:rack,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     	 => (string) $request->name,
            'catalog'  	 => $catalog,
            'fees'     	 => (int) $request->fees,
            'periodic' 	 => (int) $request->periodic,
            'display'  	 => (int) $request->display,
            'note'     	 => (string) $request->note,
            'rack'	     => (string) $request->rack,
            'speed_domestic'  	     => (string) $request->speed_domestic,
            'speed_international'    => (string) $request->speed_international,
            'bandwidth'  => (string) $request->bandwidth,
            'ip'  		 => (int) $request->ip,
            'services'   => (string) $request->services,
            'link'   => (string) $request->link,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('rack')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới tủ rack thành công!');
            return redirect()->route('admin.rack.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới tủ rack không thành công!');
            return redirect()->route('admin.rack.index');
        }
    }

    public function edit($id)
    {
        $rack = DB::table('rack')->where('id', $id)->first();
        if (isset($rack)) {
            $data['data'] = $rack;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.rack.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có tủ rack này!');
        return redirect()->route('admin.rack.index');
    }

    public function update(Request $request, $id)
    {
       $rack = DB::table('rack')->where('id', $id)->first();
        if (isset($rack)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($rack->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'          => (string) $request->name,
                'catalog'    => $catalog,
                'fees'       => (int) $request->fees,
                'periodic'   => (int) $request->periodic,
                'display'    => (int) $request->display,
                'note'       => (string) $request->note,
                'rack'       => (string) $request->rack,
                'speed_domestic'         => (string) $request->speed_domestic,
                'speed_international'    => (string) $request->speed_international,
                'bandwidth'  => (string) $request->bandwidth,
                'ip'         => (int) $request->ip,
                'services'   => (string) $request->services,
                'link'   => (string) $request->link,
                'location'   => $location,
                'created_at' => $date_time,
                'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('rack')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $rack->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin tủ rack thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin tủ rack không thành công!');
            }
            return redirect()->route('admin.rack.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có tủ rack này!');
        return redirect()->route('admin.rack.index');
    }

    public function delete($id)
    {
        $rack = DB::table('rack')->where('id', $id)->first();
        if (isset($rack)) {
            $function = DB::table('rack')->where('id', $id)->delete();
            if ($function) {
                if ($rack->catalog != 0) {
                    DB::table('catalog')->where('id', $rack->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa tủ rack thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa tủ rack không thành công!');
            }
            return redirect()->route('admin.rack.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có tủ rack này!');
        return redirect()->route('admin.rack.index');
    }
}
