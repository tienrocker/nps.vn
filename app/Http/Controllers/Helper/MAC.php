<?php

namespace App\Http\Controllers\Helper;
use DB;
use Illuminate\Support\Str;

class MAC {

	public static function getCate() {
		$category = DB::table('category')->where('display', 1)->get();
		return $category;
	}

	public static function cateColInList($id, $list, $col) //function lấy giá trị cột - $col trong 1 cate (áp dụng lại danh sách cate truyền vào)
	{
		$list = (object) $list;
		$result = '';
		if ($id == 0) {
			$result = 'No';
		} else {
			foreach ($list as $key => $val) {
				if ($id == $val->id) {
					$cate = $val;
					$result = $cate->$col;
					break;
				}
			}
		}
		return $result;
	}

	public static function redirect_page($page) {
		$category = DB::table('category')->where('slug', $page)->where('display', 1)->where('type', 'page')->first();
		if (!empty($category)) {
			$result = (object) [
				'status' => '200',
				'slug' => $category->slug . '.html',
			];
		} else {
			$result = (object) [
				'status' => '201',
				'slug' => '404',
			];
		}
		return $result;
	}

	public static function getPage() {
		$page = DB::table('pages')->where('display', 1)->orderBy('stt', 'asc')->get();
		return $page;
	}

	public static function getConfig($page) {
		foreach (config('widget.' . $page) as $item => $element) {
			foreach ($element['data'] as $key => $val) {
				$config_name[] = $val['name'];
			}
		}
		$config_data = DB::table('config')->select('name', 'value')->whereIn('name', $config_name)->get();
		foreach ($config_data as $item) {
			$widget_home[$item->name] = $item->value;
		}
		$widget_home = (object) $widget_home;
		return $widget_home;
	}

	public static function getSeo() {
		$seo = DB::table('seo')->where('display', 1)->get();
		return $seo;
	}

	public static function getSeoByPage($seo_page) {
		$seo_home = DB::table('seo')->where('display', 1)->get();
		$seo = [];
		foreach ($seo_home as $item) {
			if (!empty($seo_page)) {
				foreach ($seo_page as $key => $val) {
					$name = 'seo_' . Str::slug($item->name, '_');
					if ($name == $key) {
						$seo[$item->name] = $val;
					}
				}
			} else {
				$seo[$item->name] = $item->value;
			}

		}
		return $seo;
	}

	public static function getSeoByName($name) {
		$seo = DB::table('seo')->where('name', $name)->where('display', 1)->first();
		return $seo;
	}

	public static function xml() {
		$fp = @fopen('../public/sitemapnps.xml', 'w');
		if (!$fp) {
			return false;
		} else {
			$page = DB::table('pages')->where('display', 1)->select('id', 'slug', 'type', 'link', 'priority')->get();
			$data = '<?xml version="1.0" encoding="UTF-8" ?>';
			$data .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
			$data .= '<url>
                            <loc>' . route('home') . '</loc>
                            <lastmod>' . date(DATE_ATOM) . '</lastmod>
                            <priority>1.00</priority>
                        </url>';
			foreach ($page as $item) {
				if ($item->type != 'taxonomy' || !empty($item->link)) {
					$data .= '<url>
                                    <loc>' . route('home') . '/' . @$item->slug . '.html</loc>
                                    <lastmod>' . date(DATE_ATOM) . '</lastmod>
                                    <priority>' . @$item->priority . '</priority>
                                </url>';
				}
			}
			$data .= '</urlset>';
			fwrite($fp, $data);
			return true;
		}
	}

}
