<?php
return [
		'html' => [
			'product' => 'Trang sản phẩm',
			'lien_he' => 'Trang liên hệ',
			'gioi_thieu' => 'Trang giới thiệu',
		],
		'type' => [
			'taxonomy' 	=> 'Danh mục điều hướng',
			'product'	=> 'Sản phẩm',
			'post'		=> 'Bài viết',
			'page'		=> 'Trang'
		],
		'parameter' => [
			'hosting' => [
				'storage' 		=> 'Dung lượng lưu trữ',
				'bandwidth'		=> 'Băng thông',
				'domain'		=> 'Tên miền',
				'pack_domain'	=> 'Tên miền phụ',
				'database'		=> 'Cơ sở dữ liệu',
				'ftp_account'	=> 'FTP_Acount',
				'email'			=> 'Email',
				'ssl'			=> 'SSL',
				'ip'			=> 'IP',
				'services'		=> 'Dịch vụ',
			],
			'vps' => [
				'cpu' 		=> 'CPU',
				'ram'		=> 'RAM',
				'disk'		=> 'Ổ đĩa',
				'storage' 	=> 'Dung lượng lưu trữ',
				'bandwidth'	=> 'Băng thông',
				'OS'		=> 'Hệ điều hành',
				'services'	=> 'Dịch vụ',
			],
			'dedicated' => [
				'cpu' 		=> 'CPU',
				'ram'		=> 'RAM',
				'disk'		=> 'Ổ đĩa',
				'storage' 	=> 'Dung lượng lưu trữ',
				'bandwidth'	=> 'Băng thông',
				'OS'		=> 'Hệ điều hành',
				'speed'		=> 'Tốc độ',
				'services'	=> 'Dịch vụ',
			],
			'colocation' => [
				'rack' 					=> 'Rack',
				'speed_domestic'		=> 'Tốc độ trong nước',
				'speed_international'	=> 'Tốc độ quốc tế',
				'ip' 					=> 'Dung lượng lưu trữ',
				'services'				=> 'Dịch vụ',
			],
			'rack' => [
				'rack' 					=> 'Rack',
				'speed_domestic'		=> 'Tốc độ trong nước',
				'speed_international'	=> 'Tốc độ quốc tế',
				'bandwidth'				=> 'Băng thông',
				'ip' 					=> 'Dung lượng lưu trữ',
				'services'				=> 'Dịch vụ',
			],
			'nwall' => [
				'connection' 	=> 'Connection',
				'rule'			=> 'Rule',
				'dashboard'		=> 'Dashboard',
				'bandwidth'		=> 'Băng thông',
				'api'			=> 'API',
				'services'		=> 'Dịch vụ',
			],
		]
];