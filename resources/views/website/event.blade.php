@php
    use App\Http\Controllers\Helper\MAC;
    $page = MAC::getPage();
    $seo = MAC::getSeoByPage(json_decode(@$page_data->meta));
    $widget_app = MAC::getConfig('app');
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ @$widget_app->favicon }}">
    <title>{{ @($page_title) ? $page_title : $widget_app->title_app }}</title>
    <link rel="stylesheet" href="{{ route('home') }}/css/bootstrap.min.css">
    <script src="{{ route('home') }}/website/js/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
    <script src="{{ route('home') }}/js/bootstrap.min.js" type="text/javascript"></script>

{{-- SEO --}}
@foreach ($seo as $key => $value)
<! --------- SEO {{ $key }}------------- !>
{!! $value !!}
@endforeach
</head>
<style type="text/css">
  @media (max-width: 1440px) { 
    .container{
      max-width: 800px;
    }
  }
  @media (max-width: 1024px) {
    .container{
      max-width: 618px !important;
    }
  }
  @media (max-width: 768px) {
    .container{
      max-width: 568px !important;
    }
  }
    .img {
  position: relative; }
  .img::before {
    content: '';
    display: block;
    padding-top: 30%; }
  .img img {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%; }

body {
  background-image: url("/website/images/upload/images/du-doan-ty-so-vn-malay.jpg");
  background-size: cover;
  background-position: center; }

.content {
  margin-top: 50px;
  color: #fff; }
  .content .title {
    text-align: center; }
  .content .title_h1 {
    text-align: center;
    margin: 30px 0; }
  .content .part_one .des {
    margin: 30px 0; }
  .content .part_one .title_po {
    margin: 30px 0; }
  .content .part_two {
    margin: 30px 0; }
    .content .part_two .title_two h2 {
      font-size: 1.5rem; }
    .content .part_two .des p {
      display: inline-block; }
    .content .part_two .des h1 {
      display: inline-block;
      font-size: 1rem;
      font-weight: 700;
      border-bottom: 2px solid #fff; }
      .content .part_two .des h1 a {
        color: #fff; }
        .content .part_two .des h1 a:hover {
          text-decoration: none; }
    .content .part_two .b1 p {
      display: inline-block; }
    .content .part_two .b1 h3 {
      border-bottom: 2px solid #fff;
      padding-left: 5px;
      display: inline-block;
      font-size: 1rem; }
      .content .part_two .b1 h3 a {
        color: #fff; }
        .content .part_two .b1 h3 a:hover {
          text-decoration: none; }
    .content .part_two .b3 p span {
      background-color: #E6EEFF;
      color: red; }
  .content .part_three {
    margin: 30px 0; }
    .content .part_three .title_three h2 {
      font-size: 1.5rem; }
  .content .part_four {
    margin: 30px 0; }
    .content .part_four .title_four h2 {
      font-size: 1.5rem; }
    .content .part_four .des p {
      margin: 10px 0; }
    .content .part_four .des .nps {
      display: inline-block; }
    .content .part_four .des h3 {
      padding-left: 5px;
      display: inline-block;
      font-size: 1rem;
      font-weight: bold;
      border-bottom: 2px solid #fff; }
      .content .part_four .des h3 a {
        color: #fff; }
        .content .part_four .des h3 a:hover {
          text-decoration: none; }
  .content footer {
    margin-top: 50px;
    text-align: left;
    padding-bottom: 100px; }
    .content footer .title {
      text-align: left; }
      .content footer .title h3 {
        font-size: 1.5rem; }

/*# sourceMappingURL=style.css.map */

</style>
<body>
    <div class="img">
        <a href="https://www.facebook.com/npsgiaitri/photos/a.488170831929922/542254419854896">
            <img src="https://www.nps.vn//website/images/upload/images/du-doan-ty-so-vietnam-malaysia.jpg" alt="">
        </a>
    </div>
    <div class="container content">
        <div class="title"><h2>CHƠI 1 PHÚT - LÚT NGAY 11 PRO MAX</h2></div>
        <div class="title_h1"><h4>🎁 TỔNG TRỊ GIÁ QUÀ TẶNG LÊN TỚI HÀNG TRĂM TRIỆU ĐỒNG 🎁</h4></div>
        <div class="part_one">
            <div class="des">
                <p>NPS đi cùng đội tuyển Việt Nam tại Vòng loại World Cup 2022. Trận đấu truyền thuyết được diễn ra tại sân vận động Mỹ Đình vào lúc 20h ngày 10/10 giữa Rồng Thần Việt Nam và Hổ Châu Á Mã Lai. Liệu cổ động viên Việt Nam có được một ngày bùng nổ, thăng hoa cảm xúc cùng đội tuyển?
                        Để hòa nhịp cùng không khí ngày 10/10, NPS tổ chức Minigame “Dự đoán tỉ số trận đấu Việt Nam – Malaysia” từ ngày 4/10 – 10/10 với tổng giải thưởng lên đến hàng trăm triệu đồng! Bao gồm các phần quà:</p>
            </div>
            <div class="title_po"><h4>GIẢI ĐẶC BIỆT: 📱IPHONE 11 PRO MAX 512GB📱 trị giá 42.000.000 VNĐ</h4></div>
            <ul>
                <li><strong> 1 Giải Nhất :</strong> Tai nghe AirPods Bluetooth không dây trị giá 6.000.000 VNĐ</li>
                <li><strong> 3 Giải Nhì  : </strong>Cặp vé xem phim trị giá 600.000 VNĐ</li>
                <li><strong> 5 Giải Ba :</strong> Thẻ cào 50K trị giá 250.000 VNĐ </li>
            </ul>
        </div>
        <div class="part_two">
            <div class="title_two">
                <h2>1.	CÁCH THỨC THAM GIA:</h2>
            </div>
            <div class="des">
                <p>Để tham gia “<h1><a href="https://www.facebook.com/npsgiaitri/photos/a.488170831929922/542254419854896">Dự đoán tỉ số trận đấu Việt Nam – Malaysia</a> </h1> ”, các bạn vui lòng thực hiện đầy đủ các bước sau:</p>
            </div>
            <div class="b1">
                <p>- Bước 1:  Like Fanpage  <h3><a href="https://www.facebook.com/npsgiaitri/">NPS Entertainment</a> </h3> </p>
            </div>
            <div class="b2">
                <p>
                    - Bước 2: Like bài viết và comment theo cú pháp: <br>
                    Tỉ số - Tên cầu thủ ghi bàn đầu tiên - Phút ghi bàn - Tag 2 người <br>
                    VD: VN 5 - 0 ML - Quang Hải - Phút 89 - @XY @YZ
                </p>
            </div>
            <div class="b3">
                <p>- Bước 3: Share bài viết ở chế độ công khai KÈM bộ hashtag <span>#npsentertainment </span> <span> #WorldCup2022</span> </p>
            </div>
        </div>
        <div class="part_three">
            <div class="title_three">
                <h2>2.	THỂ LỆ NHẬN GIẢI</h2>
            </div>
            <div class="gdb">
                <p>- Giải đặc biệt: Áp dụng cho bình luận dự đoán đúng: Tỉ số - Tên cầu thủ ghi bàn đầu tiên - Phút ghi bàn đầu tiên. </p>
                <p>-	Giải nhất: Áp dụng cho bình luận dự đoán đúng: Tỉ số - Phút ghi bàn đầu tiên.</p>
                <p>
                    -	Các giải còn lại áp dụng cho các bình luận và dự đoán đúng: Tỉ số.
                </p>
            </div>
        </div>
        <div class="part_four">
            <div class="title_four">
                <h2>3.	LƯU Ý: </h2>
            </div>
            <div class="des">
                <p>-	Kết quả sẽ được công bố công khai trên Fanpage NPS Entertainment vào ngày 11/10, các bạn vui lòng theo dõi và chủ động liên hệ với chúng tôi khi đã nhận được kết quả.</p>
                <p><b>-	Trường hợp nhiều người dự đoán đúng trùng nhau, NPS sẽ tiến hành livestream và bóc thăm tên chủ nhân nhận giải thưởng.</b></p>
                <p>-	Mỗi người chỉ được comment một lần duy nhất, không qua chỉnh sửa và thực hiện đầy đủ các bước tham gia</p>
                <p>-	NPS có quyền sử dụng hình ảnh của người trúng thưởng để phục vụ cho các chiến dịch Marketing.</p>
                <p class="nps">-	Trong vòng <b>30 ngày</b> , nếu người trúng thưởng không xác nhận, <h3><a href="https://www.nps.vn/">NPS</a> </h3> sẽ hủy tư cách trúng thưởng và nhường cơ hội cho người khác. </p>
                <p>Tham gia ngay cùng chúng tôi để nhận về những giải thưởng cực kỳ hấp dẫn nhé!</p>
            </div>
        </div>
        <footer>
            <div class="title"><h3>Công Ty TNHH Công Nghệ NPS</h3></div>
            <div class="des">
                <p>Địa chỉ: 46 Dương Bá Trạc, Phường 2, Quận 8, Tp.HCM</p>
                <p>Hotline: 0825.44.55.66</p>
                <p>Điện thoại: 028 7309 2939</p>
            </div>
        </footer>
    </div>
    <div id="messeger">
        <a href="http://m.me/350741888430166" target="_blank"><i class="fab fa-facebook-messenger"></i></a>
    </div>
</body>
</html>