@extends('website.index')
@section('main')
<div class="hm-container-fluid banner-1 post">
    <div class="hm-container">
        <div class="hm-row va-m">
            <div class="hm-col-12">
                <p>{!! $page_data->content !!}</p>
            </div>
        </div>
    </div>
</div>
@endsection