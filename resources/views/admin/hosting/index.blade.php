@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-4 card_title">
							<h4>Danh sách Hosting</h4>
						</div>
						<div class="col-md-4 text-center btn_add">
							<a href="{{ route('admin.hosting.add') }}"><i class="fas fa-plus"></i></a>
						</div>
						<div class="col-md-4 card_search">
							<form action="" method="post" accept-charset="utf-8" class="">
								@csrf
								<div class="input-group justify-content-end">
									<input type="text" class="form-control input_search" name="" value="" placeholder="Search">
									<button type="submit" class="btn btn-white"><i class="fas fa-search"></i></button>
								</div>
							</form>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Storage</th>
									<th>Bandwidth</th>
									<th>Domain</th>
									<th>Paked domain</th>
									<th>Database / My SQL</th>
									<th>Ftp Acount</th>
									<th>Email</th>
									<th>SSL</th>
									<th>Fees</th>
									<th>Periodic</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach (@$list as $item)
								<tr>
									<td>{{ @$item->id }}</td>
									<td><a href="{{ route('admin.hosting.edit', ['id' => $item->id]) }}" class="px-0">{{ @$item->name }}</a></td>
									<td>{{ @$item->storage }}</td>
									<td>{{ @$item->bandwidth }}</td>
									<td>{{ @$item->domain }}</td>
									<td>{{ @$item->pack_domain }}</td>
									<td>{{ @$item->database }}</td>
									<td>{{ @$item->ftp_account }}</td>
									<td>{{ @$item->email }}</td>
									<td>{{ @$item->ssl }}</td>
									<td>{{ @number_format($item->fees) }} VNĐ</td>
									<td>{{ @$item->periodic }} Month</td>
									<td>
										<a href="{{ route('admin.hosting.edit', ['id' => $item->id]) }}"><i class="fas fa-edit"></i></a>
										<a href="{{ route('admin.hosting.delete', ['id' => $item->id]) }}" onclick="return confirm('Bạn có chắn chắn muốn xóa hosting này không?');"><i class="fas fa-trash-alt"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $list->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection