<!DOCTYPE html>
<html lang="vi">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="{{ route('home') }}/website/images/favicon.png">
	<title></title>
	{{-- CSS --}}
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/css/fontawesome.css">
	<link rel="stylesheet" href="/madmin/css/style.css">

	{{-- SCRIPT --}}
	<script src="/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.js" type="text/javascript"></script>
	

</head>
<body>
	<div class="container-fluid">
		<div class="wrapper">
			<div class="sidebar">
				<div class="logo">
					<a href="/">MAC</a>
				</div>
				<div class="sidebar_wrapper">
					<ul class="nav flex-column">
						@foreach (config('admin_menu') as $item)
							<li class="nav-item <?=($item['segment'] == Request::segment(2)) ? 'active' : ''?>"><a href="{{ url('madmin/'.$item['url']) }}" class=""><i class="{{ $item['icon'] }}"></i><span>{{ $item['name'] }}</span></a></li>
						@endforeach
					</ul>
				</div>
				<div class="sidebar_bg" style="background-image: url('/madmin/images/sidebar.jpg');">
				</div>
			</div>
			<div class="main">
				<nav class="navbar navbar-expand-lg navbar_top">
					<div class="container-fluid">
						<div class="navbar_title">
							<a href="" class="navbar-brand">
							@foreach (config('admin_menu') as $item)
								@if ($item['segment'] == Request::segment(2))
									{{ $item['name'] }}
								@endif
							@endforeach
							</a>
						</div>
						<button type="button" class="navbar-toggler" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false"><i class="fas fa-outdent"></i></button>
						<div class="collapse navbar-collapse justify-content-end">
							<ul class="navbar-nav">
								<li class="nav-item dropdown notification">
									<a href="#" id="dropdown_notify" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bell"></i><span class="number_notify">5</span></a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_notify">
										<div class="top_title row">
											<span class="col-md-4 px-0">Thông báo</span>
											<div class="col-md-8 px-0 text-right">
												<a href="">Đánh dấu tất cả là đã đọc</a>
											</div>
										</div>
										<div class="notify_board">
											<div class="new_title">
												<span class="text-muted">MỚI</span>
											</div>
											<ul class="new_notify">
												@php
													$text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
												@endphp
												@for ($i = 0; $i < 20; $i++)
												<li>
													<div class="notify_content">
														<a href=""><?=(strlen($text) > 100) ? substr($text, 0, 100) . '...' : $text?></a>
													</div>
													<div class="notify_option row">
														<div class="notify_time col-md-6 px-0">
															<span><?=date('H:i:s d-m-Y')?></span>
														</div>
														<div class="col-md-6 px-0">
															<ul class="notify_action px-0">
																<li class="d-inline"><a href=""><i class="fas fa-eye"></i></a></li>
																<li class="d-inline"><a href=""><i class="fas fa-trash-alt"></i></a></li>
															</ul>
														</div>
													</div>
												</li>
												@endfor
											</ul>
										</div>
									</div>
								</li>
								<li class="nav-item dropdown dropdown_user">
									<a href="" id="dropdown_user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>
									<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_user">
										<li>
											<a href=""><span>Profile</span><i class="fas fa-id-card"></i></a>
										</li>
										<li class="border-top">
											<a href="{{ route('logout') }}"><span>Logout</span><i class="fas fa-sign-out-alt"></i></a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</nav>
				<div class="content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</body>

<script src="/js/fontawesome.js" type="text/javascript" charset="utf-8"></script>
<script src="/madmin/js/style.js" type="text/javascript" charset="utf-8"r></script>
<script type="text/javascript">
	var formMac0 =  Array.from(document.querySelectorAll('.form_mac'));
		formMac0.forEach(function(el){
			if(Array.from(el.children)[1] != undefined){
				el.classList.add('form-mac-input');
			}
		})

	var formMac =Array.from(document.querySelectorAll('.form_mac.form-mac-input'));
		formMac.forEach(function(el){
		var labelMac = el.querySelector('.label_mac'),
			inputMac = el.querySelector('.input_mac')
		if(inputMac.value != ""){
			labelMac.style.transition = "all .3s ease";
			labelMac.style.top = "-20px";
		}
		inputMac.addEventListener('click', function(){
			labelMac.style.transition = "all .3s ease";
			labelMac.style.top = "-20px";
		})
	})
</script>
</html>