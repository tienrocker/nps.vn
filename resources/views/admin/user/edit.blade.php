@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Cập nhật thông tin người dùng</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Username</label>
								<input type="text" name="username" class="form-control input_mac" value="{{ $user->name }}" maxlength="50">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Email</label>
								<input type="email" name="email" class="form-control input_mac" value="{{ $user->email }}" maxlength="50" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Họ và tên</label>
								<input type="text" name="fullname" class="form-control input_mac" value="{{ $user->fullname }}" maxlength="200" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Số điện thoại</label>
								<input type="number" step="any" name="phone" class="form-control input_mac" value="{{ $user->phone }}" maxlength="12" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Địa chỉ</label>
								<input type="text" name="address" class=" form-control input_mac" value="{{ $user->address }}" maxlength="200">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<select name="status" class="form-control select_mac">
									@foreach (config('user_status') as $item)
									<option value="{{ $item }}" {{ ($item == $user->status) ? 'selected' : '' }}>{{ $item }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">Công ty</label>
								<input type="text" name="company" class="form-control input_mac" value="{{ $user->company }}" maxlength="200">
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">Thành phố</label>
								<input type="text" name="city" class="form-control input_mac" value="{{ $user->city }}" maxlength="200">
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Quốc gia</label>
								<input type="text" name="country" class="form-control input_mac" value="{{ $user->country }}" maxlength="200">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								<label class="label_mac">Description</label>
								<textarea name="description" class="form-control input_mac" rows="1" maxlength="2000">{{ $user->description }}</textarea>
							</div>
						</div>
						<div class="form-group row password_form">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Mật khẩu</label>
								<input type="password" name="password" class="form-control input_mac" value="" >
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Nhập lại mật khẩu</label>
								<input type="password" name="password_confirmation" class="form-control input_mac" value="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0">
								<button type="button" class="btn btn-secondary change_pass_btn">Thay đổi mật khẩu</button>
								<input type="checkbox" name="change_pass" class="change_pass d-none" value="0" >
							</div>
							<div class="col-md-6 pr-0 text-right">
								<button type="submit" class="btn">Cập nhật thông tin</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

		$('.change_pass_btn').click(function() {
			if ($('.change_pass').is(':checked')) {
				$('.change_pass').prop('checked', false);
				$('.change_pass').val(0);
				$('.change_pass_btn').html('Thay đổi mật khẩu');
				$('.password_form').css({'display':'none'});
				$('[name = "password"]').removeAttr('minlength maxlength required');
				$('[name = "password_confirmation"]').removeAttr('minlength maxlength required');
			}else{
				$('.change_pass').prop('checked', true);
				$('.change_pass').val(1);
				$('.change_pass_btn').html('Bỏ qua');
				$('.password_form').css({'display':'flex'});
				$('[name = "password"]').attr({minlength:'8', maxlength:'16', required:''});
				$('[name = "password_confirmation"]').attr({minlength:'8', maxlength:'16', required:''});
			}
		});
	});
</script>
@endsection