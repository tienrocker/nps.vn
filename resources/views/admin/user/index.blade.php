@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-4 card_title">
							<h4>Danh sách người dùng</h4>
						</div>
						<div class="col-md-4 text-center btn_add">
							<a href="{{ route('admin.user.add') }}"><i class="fas fa-plus"></i></a>
						</div>
						<div class="col-md-4 card_search">
							<form action="" method="post" accept-charset="utf-8" class="">
								@csrf
								<div class="input-group justify-content-end">
									<input type="text" class="form-control input_search" name="" value="" placeholder="Search">
									<button type="submit" class="btn btn-white"><i class="fas fa-search"></i></button>
								</div>
							</form>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Email</th>
									<th>Created Date</th>
									<th>Login Time</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach (@$list as $item)
								<tr>
									<td>{{ @$item->id }}</td>
									<td><a href="{{ route('admin.user.edit', ['id' => $item->id]) }}" class="px-0">{{ @$item->name }}</a></td>
									<td><a href="{{ route('admin.user.edit', ['id' => $item->id]) }}" class="px-0">{{ @$item->email }}</a></td>
									<td>{{ @$item->created_at }}</td>
									<td>{{ @$item->last_login }}</td>
									<td>
										<a href="{{ route('admin.user.edit', ['id' => $item->id]) }}"><i class="fas fa-edit"></i></a>
										<a href="{{ route('admin.user.delete', ['id' => $item->id]) }}" onclick="return confirm('Bạn có chắn chắn muốn xóa người dùng này không?');"><i class="fas fa-trash-alt"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $list->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection