@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Thêm mới người dùng</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Username</label>
								<input type="text" name="username" class="form-control input_mac" value="{{ old('username') }}" maxlength="50" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Email</label>
								<input type="email" name="email" class="form-control input_mac" value="{{ old('email') }}" maxlength="50" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Mật khẩu</label>
								<input type="password" name="password" class="form-control input_mac" value="" minlength="8" maxlength="16" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Nhập lại mật khẩu</label>
								<input type="password" name="password_confirmation" class="form-control input_mac" value="" minlength="8" maxlength="16" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Họ và tên</label>
								<input type="text" name="fullname" class="form-control input_mac" value="{{ old('fullname') }}" maxlength="200" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Số điện thoại</label>
								<input type="number" step="any" name="phone" class="form-control input_mac" value="{{ old('phone') }}" maxlength="12" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								<label class="label_mac">Địa chỉ</label>
								<input type="text" name="address" class=" form-control input_mac" value="{{ old('address') }}" maxlength="200">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">Công ty</label>
								<input type="text" name="company" class="form-control input_mac" value="{{ old('company') }}" maxlength="200">
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">Thành phố</label>
								<input type="text" name="city" class="form-control input_mac" value="{{ old('city') }}" maxlength="200">
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Quốc gia</label>
								<input type="text" name="country" class="form-control input_mac" value="{{ old('country') }}" maxlength="200">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								<label class="label_mac">Description</label>
								<textarea name="description" class="form-control input_mac" rows="1" maxlength="2000">{{ old('description') }}</textarea>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Thêm mới</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection