@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-4 card_title">
							<h4>Danh sách Tủ Rack</h4>
						</div>
						<div class="col-md-4 text-center btn_add">
							<a href="{{ route('admin.rack.add') }}"><i class="fas fa-plus"></i></a>
						</div>
						<div class="col-md-4 card_search">
							<form action="" method="post" accept-charset="utf-8" class="">
								@csrf
								<div class="input-group justify-content-end">
									<input type="text" class="form-control input_search" name="" value="" placeholder="Search">
									<button type="submit" class="btn btn-white"><i class="fas fa-search"></i></button>
								</div>
							</form>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Rack</th>
									<th>Speed Domestic</th>
									<th>Speed International</th>
									<th>Bandwidth</th>
									<th>IP</th>
									<th>Services</th>
									<th>Fees</th>
									<th>Periodic</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach (@$list as $item)
								<tr>
									<td>{{ @$item->id }}</td>
									<td><a href="{{ route('admin.rack.edit', ['id' => $item->id]) }}" class="px-0">{{ @$item->name }}</a></td>
									<td>{{ @$item->rack }}</td>
									<td>{{ @$item->speed_domestic }}</td>
									<td>{{ @$item->speed_international }}</td>
									<td>{{ @$item->bandwidth }}</td>
									<td>{{ @$item->ip }}</td>
									<td>{{ @$item->services }}</td>
									<td>{{ @number_format($item->fees) }} VNĐ</td>
									<td>{{ @$item->periodic }} Month</td>
									<td>
										<a href="{{ route('admin.rack.edit', ['id' => $item->id]) }}"><i class="fas fa-edit"></i></a>
										<a href="{{ route('admin.rack.delete', ['id' => $item->id]) }}" onclick="return confirm('Bạn có chắn chắn muốn xóa Tủ Rack này không?');"><i class="fas fa-trash-alt"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $list->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection