<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNwallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nwall', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('catalog');
            $table->integer('conection');
            $table->integer('bandwidth');
            $table->string('dashboard');
            $table->string('api');
            $table->text('services');
            $table->double('fees');
            $table->integer('periodic');
            $table->boolean('display');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nwall');
    }
}
