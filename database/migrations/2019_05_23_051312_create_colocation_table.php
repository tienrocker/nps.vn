<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colocation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('catalog');
            $table->string('rack');
            $table->string('speed_domestic');
            $table->string('speed_international');
            $table->integer('ip');
            $table->text('services');
            $table->double('fees');
            $table->integer('periodic');
            $table->boolean('display');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colocation');
    }
}
